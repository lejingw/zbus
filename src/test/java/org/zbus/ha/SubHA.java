package org.zbus.ha;

import org.zbus.broker.Broker;
import org.zbus.broker.BrokerConfig;
import org.zbus.broker.ha.HaBroker;
import org.zbus.mq.Consumer;
import org.zbus.mq.Protocol;
import org.zbus.net.core.Session;
import org.zbus.net.http.Message;

import java.io.IOException;

/**
 * Created by lejing on 15/12/12.
 */
public class SubHA {
	public static void main(String[] args) throws IOException {
		//
		BrokerConfig brokerConfig = new BrokerConfig();
		brokerConfig.setTrackServerList("127.0.0.1:16666;127.0.0.1:16667");

		Broker broker = new HaBroker(brokerConfig);

		Consumer c = new Consumer(broker, "MyPubSub", Protocol.MqMode.PubSub);
		c.setTopic("sse");
		c.onMessage(new Message.MessageHandler() {
			@Override
			public void handle(Message msg, Session sess) throws IOException {
				System.out.println(msg);
			}
		});

		c.start();
	}
}
