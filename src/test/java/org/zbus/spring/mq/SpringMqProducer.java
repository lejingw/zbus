package org.zbus.spring.mq;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.zbus.mq.Producer;
import org.zbus.net.http.Message;

import java.io.IOException;

public class SpringMqProducer {
	public static void main(String[] args) throws IOException, InterruptedException {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("mq/SpringMqProducer.xml");
		context.registerShutdownHook();

		Producer producer = context.getBean(Producer.class);

		long total = 0;
		for (int i = 0; i < 50; i++) {
			long start = System.currentTimeMillis();
			Message msg = new Message();
			msg.setBody("hello world" + i);
			producer.sendSync(msg);
			long end = System.currentTimeMillis();
			total += (end - start);
			if (0 == i % 1000) {
				System.out.format("Time: %.1f\n", total * 1.0 / (i + 1));
			}
		}
		context.close();
	}
}
