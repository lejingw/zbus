package org.zbus.spring.mq;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.zbus.mq.Consumer;
import org.zbus.net.core.Session;
import org.zbus.net.http.Message;

import java.io.IOException;

public class SpringMqConsumer {
	public static void main(String[] args) throws IOException, InterruptedException {
		final AbstractApplicationContext context = new ClassPathXmlApplicationContext("mq/SpringMqConsumer.xml");
		context.registerShutdownHook();

		Consumer c = context.getBean(Consumer.class);
		c.onMessage(new Message.MessageHandler() {
			@Override
			public void handle(Message msg, Session sess) throws IOException {
				System.out.println(msg);
			}
		});
		c.start();
	}
}